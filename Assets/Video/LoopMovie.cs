using UnityEngine;
using System.Collections;

public class LoopMovie : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var mt = (MovieTexture)renderer.sharedMaterial.mainTexture;
		mt.Play();
		mt.loop=true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
